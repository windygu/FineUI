###关于FineUI
------------
基于 ExtJS 的专业 ASP.NET 控件库。

###FineUI的使命
------------
创建 No JavaScript，No CSS，No UpdatePanel，No ViewState，No WebServices 的网站应用程序。

###支持的浏览器
------------
IE 7.0+、Firefox 3.6+、Chrome 3.0+、Opera 10.5+、Safari 3.0+

###授权协议
------------
Apache License 2.0

注：ExtJS 库是在 GPL v3 协议下发布(http://www.sencha.com/license)。


###相关链接
------------
* 论坛：http://fineui.com/bbs/
* 示例：http://fineui.com/demo/
* 文档：http://fineui.com/doc/
* 下载：http://fineui.codeplex.com/

 

 
FineUI严格遵守 ExtJS 关于开源软件的规则，不再内置 ExtJS 库。

* 获取适用于 FineUI 的 ExtJS 库：http://fineui.com/bbs/forum.php?mod=viewthread&tid=3218 
* 基于 FineUI 的空项目（Net2.0 和 Net4.0 两个版本）：http://fineui.com/bbs/forum.php?mod=viewthread&tid=2123

 

 

###捐赠
------------
FineUI作为一款开源软件已经持续开发了 5 年有余并发布了 100 多个版本，并还将继续下去。如果你在商业软件中使用了FineUI，请捐赠作者以帮助FineUI的持续开发。


![FineUI](http://download-codeplex.sec.s-msft.com/Download?ProjectName=fineui&DownloadId=749499)